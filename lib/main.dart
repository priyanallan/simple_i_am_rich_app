import 'dart:io';

import 'package:flutter/material.dart';

void main() => runApp(
      MaterialApp(
        home: Scaffold(
          //Home is required as the start page
          appBar: AppBar(
            title: Text("I am Rich"),
            backgroundColor: Colors.black,
          ),
          backgroundColor: Colors.blueGrey[50],
          body: Center(
            child: Image(
              image: NetworkImage('https://s3-us-west-2.amazonaws.com/uw-s3-cdn/wp-content/uploads/sites/6/2017/11/04133712/waterfall.jpg')
          ),
          ),
        ),
      ),
    );
//File("Users/vishnupriyanallan/Pictures/pics_old/98380361-lovely-kitten-portrait-cute-kitty-kitten.jpg")